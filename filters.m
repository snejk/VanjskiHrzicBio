pkg load image

mkdir("results");

cd("test");
inlist = glob("*");

for i = 1:length(inlist)
    imname = inlist{i,1};

    img = imread(imname);

    name = strsplit(imname, ".");
    
    folder = strjoin(["../results/",name(1),"/"], "");
    mkdir(folder);
    
    filename = strjoin([folder,name(1),"_sobel.",name(2)], "");    
	imwrite(edge(img, "Sobel"), filename);
	
    filename = strjoin([folder,name(1),"_log.",name(2)], "");    
	imwrite(edge(img, "LoG"), filename);
	
    filename = strjoin([folder,name(1),"_prewitt.",name(2)], "");    
	imwrite(edge(img, "Prewitt"), filename);
	
    filename = strjoin([folder,name(1),"_canny.",name(2)], "");   
	imwrite(edge(img, "Canny"), filename);
	
	
end
cd("..")
