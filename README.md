# Information entropy measures and clustering for edge detection in medical X-Ray images 

## Introduction

Development of information theory was done for the analysis of communication channels.
Shannon introduced the central concept in this theory, namely the concept of information entropy.
Information entropy can be interpreted in various ways, but Shannon mainly thought of it as a measure for unpredictabilty of information.
The information entropy concept is so general that it was soon used in other fields besides communications.

## Theory

Our main interest here is to see how determination of the number of states inside the kernel and in the whole image influences the edge detection results.

Previous results suggested that reducing the number of states (pixel intensities) in image reduces the noise problem and gives more continuous, smooth edges.
However, collapsing the neighboring intensity pixels into single value could lead to artificial differences between the new states.
For example, if 256 states which we typically see in a gray image are collapsed into 64 states, in some cases it would happen that the states which were initially closer then new digitization step (4 in this case) would be separated.
For example, intensity values 15 and 17 would fall into different states although the original difference between them was only the half of new digitizing step.

## Results

x-ray image b0016

![Base image](/assets/b0016.png)
![Primitive clustering](/assets/b0016_E_standard_4_2_c.png)
![KMeans clustering](/assets/b0016_E_kmeans_4_2_c.png)

x-ray image b0033

![Base image](/assets/b0033.png)
![Primitive clustering](/assets/b0033_E_standard_4_2_c.png)
![KMeans clustering](/assets/b0033_E_kmeans_4_2_c.png)