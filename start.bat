@echo off
setlocal enabledelayedexpansion

set path=!CD!
set img_folder=test
set res_folder=results

set matsize_list=1 2 3
set mattype_list=c r d m
set depth_list=2 4 8 16

FOR %%i IN (!img_folder!\*.pgm) DO (
	set tmp=!path!\%%i
	set tmp="!tmp:.pgm=!"
	set tmp=!tmp:%img_folder%=%res_folder%!
	md !tmp!
	
	FOR %%j IN (%matsize_list%) DO (
		FOR %%z IN (%mattype_list%) DO (
			FOR %%x IN (%depth_list%) DO (
				call binf.exe %%i %%j %%z %%x
				
				FOR %%n IN (!img_folder!\*_B_*.pgm) DO (
					move %%n !tmp!
					)
				)
			)
		)
	)
)

pause
echo on