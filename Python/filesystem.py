from __future__ import print_function, division
import os
import fnmatch
import ntpath

class FileSystem():
    """Class for working with files and folders"""

    def path_exists(self, path):
        """Method which checks file/folder existence"""
        return os.path.exists(path)


    def make_folder(self, path):
        """Method which creates folder if not exists"""
        if not self.path_exists(path):
            os.makedirs(path)


    def get_filename(self, filename):
        base, ext = os.path.splitext(filename)
        return ntpath.basename(base)


    def create_path(self, path, file):
        """Method which merges path"""
        return os.path.join(path, file)


    def walk_files(self, directory, match='*'):
        """Recursive function which gets all pictures"""
        for root, dirs, files in os.walk(directory):
            for filename in fnmatch.filter(files, match):
                yield os.path.join(root, filename)
