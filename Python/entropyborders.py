#pylint: skip-file

from __future__ import print_function, division
from filesystem import FileSystem
from pgmimage import PGMImage
from clustering import Clustering
import config as cfg
from math import log, log2, floor, ceil
import numpy as np
from time import strftime

class EntropyBorder():

    def __init__(self, filename, clusterMethod, l, s, m):
        # Choose depth levels, matrix size, matrix type and clustering method in config.py file
        self.l = l                                  # Depth levels
        self.s = s                                  # Matrix size
        self.m = m                                  # Matrix type
        self.clusterMethod = clusterMethod          # Clustering method

        self.img = PGMImage(path=filename)

        self.fs = FileSystem()


    def calculate(self):
        # Reading image
        image = self.img.read_pgm_pil()
        image = image.flatten()

        # Getting image size  
        w = int(self.img.width)
        h = int(self.img.height)

        # Initializing matrices
        main = [0 for i in range(w * h)]
        comm = [0 for i in range(w * h)]
        join = [0 for i in range(w * h)]
        cond = [0 for i in range(w * h)]

        # Getting min and max values of an image for prescaler calculation
        min = min(image)
        max = max(image)
        prescaler = self.l / (max - min)

        kernelLen = (2 * self.s + 1) * self.s

        # Call appropriate clustering method and assign index method for later use
        if cfg.ADAPTIVE_MODE == "image":
            # Instantiate clustering class
            clusters = Clustering(image)

            if self.clusterMethod == "kmeans":
                clusters.kmeansCluster()
                clusterIndex = clusters.kmeansIndex
            elif self.clusterMethod == "agglomerative":
                clusters.agglomerativeCluster()
                clusterIndex = clusters.agglomerativeIndex
            elif self.clusterMethod == "bucket":
                clusters.bucketsCluster()
                clusterIndex = clusters.bucketIndex
            elif self.clusterMethod == "spectral":
                clusters.spectralCluster()
                clusterIndex = clusters.spectralIndex
            elif self.clusterMethod == "standard":
                clusterIndex = lambda pixelValue: ceil((pixelValue - min) * prescaler)
            else:
                print ("Error, no such method exists.\n")
                return
        else:
            xCluster = Clustering()
            yCluster = Clustering()

        for x in range(self.s, h - 2 * self.s):
            for y in range(self.s, w - 2 * self.s):
                xk = []
                yk = []
                xtemp = []
                ytemp = []

                # Kernels
                if self.m == 'c':
                    for i in range(2 * self.s + 1):
                        for j in range(self.s):
                            xPixelVal = image[w * (y - self.s + i) + (x - self.s + j)]
                            yPixelVal = image[w * (y - self.s + i) + (x + self.s - j)]
                            if self.adaptiveMode == "image":
                                xk.append(clusterIndex(xPixelVal))
                                yk.append(clusterIndex(yPixelVal))
                            elif self.adaptiveMode == "kernel":
                                xtemp.append(xPixelVal)
                                ytemp.append(yPixelVal)

                elif self.m == 'r':
                    for i in range(self.s):
                        for j in range(2 * self.s + 1):
                            xPixelVal = image[w * (y - self.s + i) + (x - self.s + j)]
                            yPixelVal = image[w * (y + self.s - i) + (x - self.s + j)]
                            if self.adaptiveMode == "image":
                                xk.append(clusterIndex(xPixelVal))
                                yk.append(clusterIndex(yPixelVal))
                            elif self.adaptiveMode == "kernel":
                                xtemp.append(xPixelVal)
                                ytemp.append(yPixelVal)

                elif self.m == 'd':
                    for i in range(2 * self.s + 1, 0, -1):
                        for j in range(i):
                            xPixelVal = image[w * (y + self.s - i) + (x - self.s + j)]
                            yPixelVal = image[w * (y + self.s - j) + (x + self.s - i)]
                            if self.adaptiveMode == "image":
                                xk.append(clusterIndex(xPixelVal))
                                yk.append(clusterIndex(yPixelVal))
                            elif self.adaptiveMode == "kernel":
                                xtemp.append(xPixelVal)
                                ytemp.append(yPixelVal)

                elif self.m == 'm':
                    for i in range(2 * self.s + 1):
                        for j in range(i + 1, 2 * self.s + 1):
                            xPixelVal = image[w * (y - self.s + i) + (x - self.s + j)]
                            yPixelVal = image[w * (y - self.s + j) + (x - self.s + i)]
                            if self.adaptiveMode == "image":
                                xk.append(clusterIndex(xPixelVal))
                                yk.append(clusterIndex(yPixelVal))
                            elif self.adaptiveMode == "kernel":
                                xtemp.append(xPixelVal)
                                ytemp.append(yPixelVal)

                if self.adaptiveMode == "kernel":
                    if self.clusterMethod == "kmeans":
                        xCluster.kmeansCluster(xtemp)
                        yCluster.kmeansCluster(ytemp)
                        xClusterIndex = xCluster.kmeansIndex
                        yClusterIndex = yCluster.kmeansIndex
                    elif self.clusterMethod == "agglomerative":
                        xCluster.agglomerativeCluster(xtemp)
                        yCluster.agglomerativeCluster(ytemp)
                        xClusterIndex = xCluster.agglomerativeIndex
                        yClusterIndex = yCluster.agglomerativeIndex
                    elif self.clusterMethod == "spectral":
                        xCluster.spectralCluster(xtemp)
                        yCluster.spectralCluster(ytemp)
                        xClusterIndex = xCluster.spectralIndex
                        yClusterIndex = yCluster.spectralIndex
                    elif self.clusterMethod == "bucket":
                        xCluster.bucketsCluster(xtemp)
                        yCluster.bucketsCluster(ytemp)
                        xClusterIndex = xCluster.bucketIndex
                        yClusterIndex = yCluster.bucketIndex

                    xk = [xClusterIndex(pixel) for pixel in xtemp]
                    yk = [yClusterIndex(pixel) for pixel in ytemp]

                xyp = [0 for i in range(kernelLen)]
                xp = [0 for i in range(kernelLen)]
                yp = [0 for i in range(kernelLen)]

                # Counting repetitions
                for k in range(kernelLen):
                    for l in range(k + 1):
                        if xk[k] == xk[l]:
                            xp[l] += 1
                            break

                for k in range(kernelLen):
                    for l in range(k + 1):
                        if yk[k] == yk[l]:
                            yp[l] += 1
                            break

                for k in range(kernelLen):
                    for l in range(k + 1):
                        if xk[k] == xk[l] and yk[k] == yk[l]:
                            xyp[l] += 1
                            break

                # Calculating percentages
                xlog = 0
                ylog = 0
                xylog = 0
                for i in range(kernelLen):
                    if xlog == 0 and xp[i] == 0 and i > 1:
                        xlog = log2(i)
                    xp[i] /= kernelLen

                    if ylog == 0 and yp[i] == 0 and i > 1:
                        ylog = log2(i)
                    yp[i] /= kernelLen

                    if xylog == 0 and xyp[i] == 0 and i > 1:
                        xylog = log2(i)
                    xyp[i] /= kernelLen

                if xlog < 1:
                    xlog = 1.0
                if ylog < 1:
                    ylog = 1.0
                if xylog < 1:
                    xylog = 1.0

                # Calculating entropy
                ex = 0
                ey = 0
                j = 0
                for i in range(kernelLen):
                    if xp[i]:
                        ex -= xp[i] * log2(xp[i])
                    if yp[i]:
                        ey -= yp[i] * log2(yp[i])
                    if xyp[i]:
                        j -= xyp[i] * log2(xyp[i])

                # Format conversion (0-255)
                ex = (ex / xlog) * 255
                ey = (ey / ylog) * 255
                j = (j / xylog) * 255

                main[y * w + x] = floor(ex)
                join[y * w + x] = floor(j)
                cond[y * w + x] = floor(j - ey)
                comm[y * w + x] = floor(ex + ey - j)

        # Saving image
        save_folder = self.fs.create_path(cfg.RESULTS_DIR, self.fs.get_filename(self.img.img_path))
        self.fs.make_folder(save_folder)

        self.img.save_pgm(main, self.fs.create_path(save_folder, "%s_E_%s_%d_%d_%c.pgm" % (self.fs.get_filename(self.img.img_path), self.clusterMethod, self.l, self.s, self.m)))
        self.img.save_pgm(cond, self.fs.create_path(save_folder, "%s_C_%s_%d_%d_%c.pgm" % (self.fs.get_filename(self.img.img_path), self.clusterMethod, self.l, self.s, self.m)))
        self.img.save_pgm(join, self.fs.create_path(save_folder, "%s_J_%s_%d_%d_%c.pgm" % (self.fs.get_filename(self.img.img_path), self.clusterMethod, self.l, self.s, self.m)))
        self.img.save_pgm(comm, self.fs.create_path(save_folder, "%s_I_%s_%d_%d_%c.pgm" % (self.fs.get_filename(self.img.img_path), self.clusterMethod, self.l, self.s, self.m)))
        print (strftime("%H:%M:%S"), "Done!")


if __name__ == "__main__":
    fs = FileSystem()

    if fs.path_exists(cfg.IMAGES_DIR):
        for filename in fs.walk_files(cfg.IMAGES_DIR, '*.pgm'):
            print (strftime("%H:%M:%S"), "Working on:", filename, end=" ")
            for clusterMethod in cfg.CLUSTER_TYPE:
                print("| Clustering method:", clusterMethod, end=" ")
                for l in cfg.LEVEL_NUM:
                    print ("| Depth levels:", l, end=" ")
                    for s in cfg.MATRIX_SIZE:
                        print ("| Matrix size:", s, end=" ")
                        for m in cfg.MATRIX_TYPE:
                            print ("| Matrix type:", m)
                            border = EntropyBorder(filename, clusterMethod, l, s, m)
                            border.calculate()