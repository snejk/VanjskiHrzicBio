#pylint: skip-file

from __future__ import print_function, division
import numpy
from PIL import Image
import numpy as np

class PGMImage():
    """Class for working with PGM images"""

    def __init__(self, path=None):
        """Init function. Reads image"""
        if path:
            self.img_path = path
            self.img_matrix = self.read_pgm_pil()
            
    def read_pgm_pil(self):
        """Return image data from a raw PGM file using PIL as numpy array."""

        image = Image.open(self.img_path)
        im = np.array(image)
        self.width = image.width
        self.height = image.height
        return im


    def save_pgm(self, matrix, path):
        """Save image as pgm file"""
        try:
            fout=open(path, 'w')
        except IOError as er:
            print ('Cannot open file ', path, 'Exiting\n', er)
            return

        pgmHeader = 'P2' + '\n' + str(int(self.width)) + ' ' + str(int(self.height)) + '\n' + str(255) + '\n'

        fout.write(pgmHeader)

        for r in matrix:
            if r < 0:
                r = 0
            elif r > 255:
                r = 255
            fout.write(str(int(r)) + "\n")
        fout.close()


    def get_matrix(self):
        return self.img_matrix