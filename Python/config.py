#IMAGES_DIR = "images/"
IMAGES_DIR = "test/"
RESULTS_DIR = "results/"

LEVEL_NUM = [4]

MATRIX_SIZE = [16]
# MATRIX_TYPE = ['r', 'c', 'd', 'm']
MATRIX_TYPE = ['c']

# ADAPTIVE_MODE = ['image', 'kernel']
ADAPTIVE_MODE = ['kernel']

# CLUSTER_TYPE = ['kmeans', 'agglomerative', 'bucket', 'standard', 'spectral']
CLUSTER_TYPE = ['kmeans']

"""
#IMAGES_DIR = "images/"
IMAGES_DIR = "test/"
RESULTS_DIR = "results/"

LEVEL_NUM = [2, 4, 8, 16, 32]

MATRIX_SIZE = [3, 5, 7]
MATRIX_TYPE = ['r', 'c', 'd', 'm']
"""