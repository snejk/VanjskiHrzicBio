#pylint: skip-file
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
from math import log2, ceil, floor
from scipy.cluster.vq import kmeans, vq
from sklearn.cluster import SpectralClustering, AgglomerativeClustering
from scipy.spatial import distance
from scipy.cluster.hierarchy import fcluster,linkage
from sklearn.cluster import spectral_clustering
from sklearn.neighbors import NearestNeighbors

class Clustering():

    def __init__(self, image=None):
        self.__image = image


    """Private methods"""
    def __getDistances(self, data, zeros=True):
        """Get distances for values in an array"""
        """Zeros argument tells the algorithm whether to calculate the distance between the same element"""
        distances = []
        for num1 in data:
            tmp_distances = []
            for num2 in data:
                if zeros == True:
                    tmp_distances.append(abs(num1-num2))
                else:
                    if abs(num1-num2) != 0:
                        tmp_distances.append(abs(num1-num2))
            distances.append(tmp_distances)
        return distances


    def __getHistogram(self, data=None):
        """Get histogram and keep values above 95th percentile"""        
        hist = np.zeros(256)

        if data != None:
            for value in data:
                hist[int(value)] += 1
            self.__numberOfBuckets = max(2, int(log2(len(set(data)))))
            return [val for val in hist if val != 0]

        for value in self.__image:
            hist[int(value)] += 1

        percentile = 0.95
        index = floor(percentile * len(hist))
        perc = sorted(hist)[index]

        histPerc = []

        for idx, val in enumerate(hist):
            if val > perc:
                if idx not in histPerc:
                    histPerc.append(idx)

        self.__numberOfBuckets = int(log2(len(histPerc)))
        return histPerc


    """Methods for returning specific clustering method indices"""
    def kmeansIndex(self, val):
        """Acquire k-means cluster index based on the input value"""
        for idx, v in enumerate(self.__kmeansData):
            if val == v:
                return self.__kmeansIndices[idx]
        return 0


    def bucketIndex(self, val):
        """Acquire bucket cluster index based on the input value"""
        for idx, v in enumerate(self.__buckets):
            if val == v:
                return self.__bucketIndices[idx]
        return 0


    def agglomerativeIndex(self, val):
        """Acquire agglomerative cluster index based on the input value"""
        for idx, v in enumerate(self.__agglomerativeData):
            if val == v:
                return self.__agglomerativeIndices[idx]
        return 0


    def spectralIndex(self, val):
        """Acquire spectral cluster index based on the input value"""
        for idx, v in enumerate(self.__spectralData):
            if val == v:
                return self.__spectralIndices[idx]
        return 0


    """Methods for creating clusters using several algorithms"""
    def kmeansCluster(self, data=None):
        """Create clusters using k-means algorithm"""
        if data != None:
            d = self.__getHistogram(data)
        else:
            d = self.__getHistogram()

        d = np.array(d)
        self.__kmeansData = d
        d = np.asarray(d, dtype=float)

        codebook, _ = kmeans(d, self.__numberOfBuckets)
        self.__kmeansIndices, _ = vq(d, codebook)


    def agglomerativeCluster(self, data=None):
        """Create clusters using agglomerative clustering"""
        if data != None:
            d = self.__getHistogram(data)
        else:
            d = self.__getHistogram()

        d = np.array(d)
        self.__agglomerativeData = d

        # Calculating clusters and merging them
        Z = linkage(self.__getDistances(d, zeros=True), 'complete')    
        self.__agglomerativeIndices = fcluster(Z, self.__numberOfBuckets, criterion='maxclust')


    def spectralCluster(self):
        """Create clusters using spectral clustering"""
        if data != None:
            d = self.__getHistogram(data)
        else:
            d = self.__getHistogram()

        buckets = [[val] for val in d]

        d = np.array(d)
        self.__spectralData = d

        neigh = NearestNeighbors(n_neighbors=self.__numberOfBuckets)
        neigh.fit(buckets) 
        graph = neigh.kneighbors_graph(buckets)
        graph.toarray()
        self.__spectralIndices = spectral_clustering(graph, n_clusters=self.__numberOfBuckets, eigen_solver='arpack')


    def bucketsCluster(self, data=None):
        """Create clusters using "bucket" algorithm"""

        # Input
        if data != None:
            d = self.__getHistogram(data)
        else:
            d = self.__getHistogram()
        d.sort()

        # Distance calculation
        distances = self.__getDistances(d, zeros=False)

        # Average distance calculation
        avg = []
        for distance in distances:
            suma = sum([each for each in distance])
            avg.append(int(suma/(len(distance))))

        # Averaging average distances
        bound = np.average(avg)/len(d)

        # Bucket creation
        buckets = []
        i = 0
        while i < len(d)-1:
            if (d[i+1] - d[i]) <= bound:
                tmp_bucket = [d[i], d[i+1]]
                i += 1
            else:
                tmp_bucket = [d[i]]
            i += 1
            buckets.append(tmp_bucket)
        buckets.append([d[-1]])

        while len(buckets) != self.__numberOfBuckets:
            buck_dist = []
            for i in range(len(buckets)-1):
                diff = min(buckets[i+1])-max(buckets[i])
                buck_dist.append(diff)
            index = buck_dist.index(min(buck_dist))
            buckets[index] += buckets[index+1]
            del buckets[index+1]

        self.__buckets = buckets
        self.__flatBuckets = [val for sublist in self.__buckets for val in sublist]
        self.__bucketIndices = [i for i, subBucket in enumerate(self.__buckets) for v in subBucket]


if __name__ == "__main__":
    from PIL import Image
    path = r'E:\Google drive\FAX\DIPLOMSKI\Semestar 3\Bioinf\fhrzic-bioinformatika-144e70d1e068\test\b0008.pgm'
    b = Clustering(np.array(Image.open(path)).flatten())
    b.bucketsCluster()
    print(b.bucketIndex(56))