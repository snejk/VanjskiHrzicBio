#pylint: skip-file
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import sys

def main(argv):

    try:
        img1 = argv[1]
        img2 = argv[2]
    except IndexError:
        print ("Not enough arguments provided. Exiting.")
        return

    if ("diego" in img1):
        im = np.array(Image.open(img1))
        x = np.reshape(im, len(im[0])*len(im[1]))
    else:
        with open(img1, 'r') as image1:
            x = [int(line) for i, line in enumerate(image1) if i > 2]
    
    if ("diego" in img2):
        im = np.array(Image.open(img2))
        y = np.reshape(im, len(im[0])*len(im[1]))
    else:
        with open(img2, 'r') as image2:
            y = [int(line) for i, line in enumerate(image2) if i > 2]

    # Inverting image
    # with open("agg_inverted.pgm", 'w') as test:
    #     test.write("P2\n750 750\n255\n")
    #     for i in x:
    #         test.write(str(255-i)+"\n")

    # with open("spec_inverted.pgm", 'w') as test:
    #     test.write("P2\n750 750\n255\n")
    #     for i in y:
    #         test.write(str(255-i)+"\n")

    # Opening images with PIL
    # image1 = np.array(Image.open(img1))
    # image2 = np.array(Image.open(img2))
    # print (image2)

    # Plot data
    # fig1 = plt.figure()
    # plt.plot(x,y,'.r')
    # plt.xlabel(img1.split("_")[0])
    # plt.ylabel(img2.split("_")[0])

    # axes = plt.gca()
    # axes.set_xlim([0, 255])
    # axes.set_ylim([0, 255])
    
    # Estimate the 2D histogram
    nbins = 255
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins)
    
    # H needs to be rotated and flipped
    H = np.rot90(H)
    H = np.flipud(H)
    
    # Mask zeros
    Hmasked = np.ma.masked_where(H==0,H) # Mask pixels with a value of zero
    
    # Plot 2D histogram using pcolor
    fig2 = plt.figure()
    plt.pcolormesh(xedges,yedges,Hmasked)
    plt.xlabel(img1.split("_")[0])
    plt.ylabel(img2.split("_")[0])

    axes = plt.gca()
    axes.set_xlim([0, 255])
    axes.set_ylim([0, 255])

    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Counts')

    plt.show(fig2)

if __name__ == "__main__":
    main(sys.argv)