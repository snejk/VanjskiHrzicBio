#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

///Strukture sa podatcima
typedef struct picture
{
    char filename[256];
    FILE *pImg;
    uint16_t w;
    uint16_t h;
    uint16_t d;
    uint16_t newd;
    uint16_t min;
    uint16_t max;
    uint8_t *img;
} Picture;

typedef struct matrix
{
    char type;
    uint8_t msize;
} Matrix;

///Prototipovi funkcija
int load_image(Picture *Img);
int save_image(Picture *Img, Matrix *Mat, char c);
int find_borders(Picture *Img, Matrix *Mat);


int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        printf("#NEDOVOLJAN BROJ ARGUMENATA#");
        return -1;
    }
    Picture Img;

    ///Ime slike
    strcpy(Img.filename, argv[1]);
    Img.pImg = fopen(Img.filename, "r");
    if(!Img.pImg)
    {
        printf("#DATOTEKA TOG IMENA NE POSTOJI ILI NEUSPJELO UCITAVANJE SLIKE#");
        return -1;
    }

    ///Ucitavanje slike
    if(load_image(&Img) == -1)
    {
        printf("#KRIVA VRSTE SLIKE#");
        return -1;
    }

    ///Citanje broja nivoa
    Img.newd = atoi(argv[4]);
    if(Img.newd < 2 || Img.newd > 256)
    {
        printf("#KRIVI BROJ NIVOA#");
        return -1;
    }

    ///Ucitavanje velicine matrice
    Matrix Mat;
    Mat.msize = atoi(argv[2]);
    if(Mat.msize < 1)
    {
        printf("#KRIVA VELICINA MATRICE#");
        return -1;
    }

    ///Ucitavanje tipa matrice
    Mat.type = argv[3][0];
    if(Mat.type != 'r' && Mat.type != 'c' && Mat.type != 'd' && Mat.type != 'm')
    {
        printf("#KRIVI TIP MATRICE#");
        return -1;
    }

    ///Zapocinjanje pretrage rubova
    if(find_borders(&Img, &Mat) != 0)
    {
        printf("#NEUSPJELO RACUNANJE#");
        return -1;
    }

    return 0;
}

int load_image(Picture *Img)
{
    char buffer[8];
    int i=0;
    int type = 0;

    ///Ucitavanje tipa fotografije
    while((buffer[i] = fgetc(Img->pImg)) != '\n') i++;
    if(!strncmp(buffer, "P2", 2))
    {
        type = 0;
    }
    else if(!strncmp(buffer, "P5", 2))
    {
        type = 1;
    }
    else
    {
        return -1;
    }

    ///Preskakanje svih komentara
    while((buffer[0] = fgetc(Img->pImg)) == '#')
    {
        while(fgetc(Img->pImg) != '\n');
    }
    ungetc(buffer[0], Img->pImg);

    ///Ucitavanje osnovnih podataka o slici
    fscanf(Img->pImg, "%d %d\n%d\n", &Img->w, &Img->h, &Img->d);

    ///Kreiranje mjesta za sliku
    unsigned int pic_size = Img->h * Img->w;
    Img->img = (uint8_t *) malloc (pic_size);

    ///Ucitavanje slike
    if(type)
    {
        ///P5 type
        if(fread(Img->img, 1, pic_size, Img->pImg) < 0)
        {
            return -2;
        }
    }
    else
    {
        ///P2 type
        for(i=0; i<pic_size; i++)
        {
             fscanf(Img->pImg, "%d\n", &Img->img[i]);
        }
    }

    ///Oslobadjanje ulazne slike
    free(Img->pImg);

    ///Trazenje minimuma i maksimuma
    Img->min = Img->d;
    Img->max = 0;
    for(i=0; i<pic_size; i++)
    {
        if(Img->img[i] > Img->max)
        {
            Img->max = Img->img[i];
        }
        else if(Img->img[i] < Img->min)
        {
            Img->min = Img->img[i];
        }

    }

    return 0;
}

int save_image(Picture *Img, Matrix *Mat, char c)
{
    char fname[256], tmp[128];
    int i;

    ///Kreiranje imena izlazne datoteke
    strcpy(tmp, Img->filename);
    tmp[strlen(Img->filename)-4] = '\0';
    sprintf(fname, "%s_B_%c_%d_%c_%03d.pgm\0", tmp, Mat->type, Mat->msize, c, Img->newd);

    ///Otvaranje izlazne datoteke
    Img->pImg = fopen(fname, "w");
    if(!Img->pImg)
    {
        printf("#NEMOGUCE KREIRATI DATOTEKU#");
        return -1;
    }

    ///Zapis osnovnih podataka o datoteci
    fprintf(Img->pImg, "P5\n");
    fprintf(Img->pImg, "%d %d\n%d\n", Img->w, Img->h, Img->d);

    for(i=0; i<Img->w*Img->h; i++)
    {
        if(Img->img[i] == 10 || Img->img[i] == 13) Img->img[i]++;
        fprintf(Img->pImg, "%c", (unsigned char) Img->img[i]);
    }
    //fwrite((void*)Img->img, sizeof(unsigned char), (Img->h*Img->w), Img->pImg);
    fclose(Img->pImg);
    free(Img->pImg);

    return 0;
}

int find_borders(Picture *Img, Matrix *Mat)
{
    int i, j, x, y;
    uint16_t w,h,d, newd, min, max, ms;
    char mt;

    w = Img->w;
    h = Img->h;
    d = Img->d;
    newd = Img->newd;
    min = Img->min;
    max = Img->max;

    ms = Mat->msize;
    mt = Mat->type;

    ///Instanciranje slika
    Picture Entropy, Joint, Cond, Comm;
    Joint.d = Cond.d = Comm.d = Entropy.d = d;
    Joint.h = Cond.h = Comm.h = Entropy.h = h;
    Joint.w = Cond.w = Comm.w = Entropy.w = w;
    Joint.newd = Cond.newd = Comm.newd = Entropy.newd = newd;

    strcpy(Entropy.filename, Img->filename);
    strcpy(Joint.filename, Img->filename);
    strcpy(Cond.filename, Img->filename);
    strcpy(Comm.filename, Img->filename);

    Entropy.img = (uint8_t *) calloc (w * h, sizeof(uint8_t));
    Joint.img = (uint8_t *) calloc (w * h, sizeof(uint8_t));
    Cond.img = (uint8_t *) calloc (w * h, sizeof(uint8_t));
    Comm.img = (uint8_t *) calloc (w * h, sizeof(uint8_t));

    ///Instanciranje varijabli
    int xk, yk;
    uint8_t *xker, *yker;
    double *xp, *yp, *xyp;

    ///Kernel za svaku stranu
    xker = (uint8_t *) malloc ((2*ms+1) * ms);
    yker = (uint8_t *) malloc ((2*ms+1) * ms );

    ///Vjerojatnosti za svaku stranu
    xp = (double *) malloc (sizeof(double) * (2*ms+1) * ms);
    yp = (double *) malloc (sizeof(double) * (2*ms+1) * ms);

    ///Vjerojatnosti parova
    xyp = (double *) malloc (sizeof(double) * (2*ms+1) * ms);

    ///Prescaler na pravi broj nivoa
    double prescaler =  (double) newd / (max - min);


    ///Racunanje
    for(y=ms; y<h-ms; y++)
    {
        for(x=ms; x<w-ms; x++)
        {
            ///Uzimanje podataka - kernel
            xk = 0;
            yk = 0;
            switch(mt)
            {
                case 'c':
                    for(i=0; i<(2*ms+1); i++)
                    {
                        for(j=0; j<ms; j++)
                        {
                            xker[xk] = ceil((Img->img[w*(y-ms+i) + (x-ms+j)] - min) * prescaler);
                            xk++;

                            yker[yk] = ceil((Img->img[w*(y-ms+i) + (x+ms-j)] - min) * prescaler);
                            yk++;
                        }
                    }
                    break;
                case 'r':
                    for(i=0; i<ms; i++)
                    {
                        for(j=0; j<(2*ms+1); j++)
                        {
                            xker[xk] = ceil((Img->img[w*(y-ms+i) + (x-ms+j)] - min) * prescaler);
                            xk++;

                            yker[yk] = ceil((Img->img[w*(y+ms-i) + (x-ms+j)] - min) * prescaler);
                            yk++;
                        }
                    }
                    break;
                case 'd':
                    for(i=2*ms; i>=0; i--)
                    {
                        for(j=0; j<i; j++)
                        {
                            xker[xk] = ceil((Img->img[w*(y+ms-i) + (x-ms+j)] - min) * prescaler);
                            xk++;

                            yker[yk] = ceil((Img->img[w*(y+ms-j) + (x+ms-i)] - min) * prescaler);
                            yk++;
                        }
                    }
                    break;
                case 'm':
                    for(i=0; i<(2*ms+1); i++)
                    {
                        for(j=i+1; j<(2*ms+1); j++)
                        {
                            xker[xk] = ceil((Img->img[w*(y-ms+i) + (x-ms+j)] - min) * prescaler);
                            xk++;

                            yker[yk] = ceil((Img->img[w*(y-ms+j) + (x-ms+i)] - min) * prescaler);
                            yk++;
                        }
                    }
                    break;
            }

            ///Inicijalizacija polja postotaka
            for(i=0; i<(2*ms+1)*ms; i++)
            {
                xp[i] = yp[i] = xyp[i] = 0;
            }

            ///Brojanje istih ponavljanja
            for(i=0; i<(2*ms+1)*ms; i++)
            {
                ///Postoci - x
                for(j=0; j<=i; j++)
                {
                    if(xker[i] == xker[j])
                    {
                        xp[j] += 1;
                        break;
                    }
                }

                ///Postoci - y
                for(j=0; j<=i; j++)
                {
                    if(yker[i] == yker[j])
                    {
                        yp[j] += 1;
                        break;
                    }
                }

                ///Postoci - parovi
                for(j=0; j<=i; j++)
                {
                    if(xker[i] == xker[j] && yker[i] == yker[j])
                    {
                        xyp[j] += 1;
                        break;
                    }
                }
            }

            ///Logaritam za stavljanje svega na zajednicku bazu
            //double logaritam = -1 * log2f(1.f/((2*ms+1)*ms));
            double xlog, ylog, xylog;
            xlog = ylog = xylog = 0;

            ///Pretvaranje u postotke
            for(i=0; i<(2*ms+1)*ms; i++)
            {
                if(xlog == 0 && xp[i] == 0 && i > 1)
                {
                    xlog = log2f(i);
                }
                xp[i] /= (2*ms+1)*ms;

                if(ylog == 0 && yp[i] == 0 && i > 1)
                {
                    ylog = log2f(i);
                }
                yp[i] /= (2*ms+1)*ms;

                if(xylog == 0 && xyp[i] == 0 && i > 1)
                {
                    xylog = log2f(i);
                }
                xyp[i] /= (2*ms+1)*ms;
            }

            ///Inicijalizacija varijabli
            double ex, ey, j;
            ex = ey = j = 0;

            for(i=0; i<(2*ms+1)*ms; i++)
            {
                if(xp[i])
                {
                    ex -= xp[i] *  log2f(xp[i]);
                }

                if(yp[i])
                {
                    ey -= yp[i] *  log2f(yp[i]);
                }

                if(xyp[i])
                {
                    j -= xyp[i] *  log2f(xyp[i]);
                }
            }

            ///Konvertiranje u pravi format (0-255)
            ex = (ex/xlog) * d;
            ey = (ey/ylog) * d;
            j = (j/xylog) * d;

            if(ex < 0 || ey <0 || j < 0) printf("Ex=%.2f # Ey=%.2f # J=%.2f\n", ex, ey, j);

            Entropy.img[y*w + x] = floor(ex);
            Joint.img[y*w + x] = floor(j);
            Cond.img[y*w + x] = floor(j-ey);
            Comm.img[y*w + x] = floor(ex+ey-j);

            /*
            //printf("# C # %.2f\t# I # %.2f #\n", floor(j-ey), floor(ex+ey-j));
            //if(floor(j-ey) == 10 || floor(ex+ey-j) == 10 | floor(j-ey) == 13 || floor(ex+ey-j) == 13)
            if(floor(j-ey) == 10 || floor(ex+ey-j) == 10)
            {
                //printf("# C # %.2f\t# I # %.2f #\n", j-ey, ex+ey-j);
                printf("%d, %c, %d", newd, mt, ms);
                //system("PAUSE");
            }*/
        }
    }

    save_image(&Entropy, Mat, 'E');
    save_image(&Joint, Mat, 'J');
    save_image(&Cond, Mat, 'C');
    save_image(&Comm, Mat, 'I');

    free(Entropy.img);
    free(Joint.img);
    free(Cond.img);
    free(Comm.img);
    return 0;
}
